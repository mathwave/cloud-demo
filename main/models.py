from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone


class Blog(models.Model):
    name = models.TextField()


class Post(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    created_dt = models.DateTimeField(default=timezone.now)
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE, db_index=True)
    text = models.TextField()
