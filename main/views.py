from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from main.models import Blog, Post


def register(request):
    if request.method == 'POST':
        User.objects.create_user(username=request.POST['username'], password=request.POST['password'])
        return HttpResponseRedirect('/enter')
    return render(request, 'register.html')


def enter(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return HttpResponseRedirect('/enter')
        login(request, user)
        return HttpResponseRedirect('/blogs')
    return render(request, 'enter.html')


def create_blog(request):
    Blog.objects.create(name=request.POST['name'])
    return HttpResponseRedirect('/blogs')


def blogs(request):
    return render(request, 'blogs.html', context={
        'blogs': Blog.objects.all()
    })


def posts(request, blog_id: int):
    if request.method == 'POST':
        Post.objects.create(text=request.POST['text'], creator=request.user, blog_id=blog_id)
        return HttpResponseRedirect(f'/posts/{blog_id}')
    return render(request, 'posts.html', context={
        'posts': Post.objects.filter(blog_id=blog_id).order_by('-created_dt')
    })